/* --------------------------
01 - Looping a triangle
Write a loop that makes
seven calls  to console.log to
output the following triangle:
#
##
###
####
#####
######
#######
-----------------------------*/
// solution 1
for (let i = 1; i <=7; i++){
  let string = ''
  for (let n = 0; n < i; n++ ) { 
    string += '#'
  }
  console.log(string)
}

// soulution 2 - less cod but not friendly solution
for (let i = '#'; i.length <= 7; i+='#') console.log(i)

/* --------------------------------------------------------
02 - Write a program that uses console.log to print all the
numbers from 1 to 100, with two exceptions. For numbers
divisible by 3, print "Fizz" instead of the number, and
for numbers divisible by 5 (and not 3), print "Buzz" instead.

When you have that working, modify your program to
print "FizzBuzz" for numbers that are divisible by both 3 and 5
(and still print "Fizz" or "Buzz" for numbers divisible by only one of those).
---------------------------------------------------------*/
// Longer version
// for ( let i = 1; i <= 100; i++) {
//   if ( (i % 3 === 0) && (i % 5 !== 0) ) {
//     console.log('Fizz')
//     continue
//   }
//   if ( (i % 5 === 0) && (i % 3 !== 0) ) {
//     console.log('Buzz')
//     continue
//   }
//   if ( (i % 5 === 0) && (i % 3 === 0) ) {
//     console.log('FizzBuzz')
//     continue
//   }
//   console.log(i)
// }
// Shorter version
// for (let i = 1; i <= 100; i++) {
//   let output = ''
//   if (i % 3 === 0) output += 'Fizz'
//   if (i % 5 === 0) output += 'Buzz'
//   console.log(output||i) 
// }

/* --------------------------------------------------------
03 - Write a program that creates a string that represents
an 8×8 grid, using newline characters to separate lines.
At each position of the grid there is either a space or a "#"
character. The characters should form a chessboard.
Passing this string to console.log should show something like this:

 # # # #
# # # # 
 # # # #
# # # # 
 # # # #
# # # # 
 # # # #
# # # #

When you have a program that generates this pattern, define
a binding size = 8 and change the program so that it
works for any size, outputting a grid of the given width and height.
---------------------------------------------------------*/
// Solution 1
// let gridW = 8;
// let gridH = 8;
// let string = ''
// for ( let r = 1; r <= gridH; r++) {
//   // rows
//   for ( let c = 1; c <= gridW; c++) {
//     // columns
//     if (r % 2 === 0) { 
//       string += c%2 === 0 ? " " : "#" 
//     } else { 
//       string += c%2 === 0 ? '#' : " "
//     }
//   }
//   if (r !== gridH) string += '\n'
// }
// console.log(string)
// Soulution 2
let gridW = 23;
let gridH = 8;
let string = ''
for (let r = 1; r <= gridH; r++){
  //rows
  for (let c = 1; c <= gridW; c++){
    // columns
    (r + c) % 2 === 0 ? string += " " : string +="#"
  }
  if (r !== gridH) string += "\n"
}
console.log(string)