// BREAK THE LOOP
// console.warn("********** BREAK THE LOOP **********")
// for ( let i = 0; i < 5; i++ ){
//   if (i == 3) break
//   console.log(`Outer ${i}`)
//   for ( let i = 0; i < 2; i++ ){
//     if(i == 1) break
//     console.log(`Inner ${i}`)
//   }
// }

// CONTINUE LOOP
// console.warn("********** CONTINUE THE LOOP **********")
// for( let i = 1; i <= 10; i++) {
//   if (i % 2 === 0) continue
//   console.log(`Odd Num ${i}`)
// }

// console.warn("********** REMINDER **********")
// REMINDER - MODULO
// console.log(`12 % 4 = ${12%4}`)
// console.log(`20 % 5 = ${20%5}`)
// console.log(`2 % 4 = ${2%4}`)
// console.log(`1 % 4 = ${1%4}`)
// console.log(`0 % 4 = ${0%4}`)
// console.log(`NaN % 4 = ${NaN%4}`)

// console.warn("********** SWITCH **********")
// SWITCH
// let expr = "Apples"
// switch (expr) {
//   case  "Oranges":
//       console.log('Oranges are $5.45')
//       break
//   case "Apples":
//     // if expr === Apples switch will enter this case but will continue until break is found
//     console.log("Apples are $4.33")
//   case "Banannas":
//     console.log(`Bananas are $2.99`)
//   case "Mangos":
//   case "Papayas":
//     console.log("Mangos and Papayas are $3.33")
//     break
//   default:
//     console.log(`Sorry we are out of ${expr}`)
// }